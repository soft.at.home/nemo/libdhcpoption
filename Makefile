-include $(CONFIGDIR)/components.config

INSTALL ?= install
PKG_CONFIG_LIBDIR ?= $(D)/usr/lib/pkgconfig/

COMPONENT = dhcpoption

compile: 
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean

install: 
	$(INSTALL) -d $(D)/usr/include/
	$(INSTALL) -D -m 755 src/lib$(COMPONENT).so $(D)/lib/lib$(COMPONENT).so.$(PV)
	ln -sfr $(D)/lib/lib$(COMPONENT).so.$(PV) $(D)/lib/lib$(COMPONENT).so
	$(INSTALL) -p -D -m 0644 include/$(COMPONENT).h $(D)/usr/include/
	$(INSTALL) -p -D -m 0644 pkgconfig/$(COMPONENT).pc $(PKG_CONFIG_LIBDIR)/$(COMPONENT).pc

.PHONY: compile clean install

