/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <arpa/inet.h>

#include <pcb/utils.h>

#include <debug/sahtrace.h>

static char buf[3000];

static uint64_t htonll(uint64_t hostlonglong) {
	int i;
	uint8_t t, *h;

	if (htonl(10) != 10) {
		// Host is little endian, swap bytes
		h = (uint8_t *)(&hostlonglong);
		for (i = 0; i < 4; i++) {
			t = h[i];
			h[i] = h[7-i];
			h[7-i] = t;
		}
	}
	// else: Host is also big endian, don't do anything

	return hostlonglong;
}

const char *dhcpoption_parseIPv4Addr(unsigned char *binval) {
	sprintf(buf, "%u.%u.%u.%u", binval[0], binval[1], binval[2], binval[3]);
	return buf;
}

const char *dhcpoption_parseIPv6Addr(unsigned char *binval) {
	struct in6_addr addr;
	memcpy(&addr, binval, 16);
	inet_ntop(AF_INET6, &addr, buf, sizeof(buf));
	return buf;
}

uint32_t dhcpoption_parseUInt32(unsigned char *binval) {
	uint32_t retval;
	memcpy(&retval, binval, 4);
	return ntohl(retval);
}

uint16_t dhcpoption_parseUInt16(unsigned char *binval) {
	uint16_t retval;
	memcpy(&retval, binval, 2);
	return ntohs(retval);
}

int32_t dhcpoption_parseInt32(unsigned char *binval) {
	int32_t retval;
	memcpy(&retval, binval, 4);
	return ntohl(retval);
}

const char *dhcpoption_parseAscii(unsigned char *binval, int len) {
	int i;
	if (len > (int)sizeof(buf) - 1)
		len = sizeof(buf) - 1;
	for (i = 0; i < len; i++) {
		if (!binval[i])
			break;
		else if (binval[i] < 32 && binval[i] > 126)
			buf[i] = '.';
		else
			buf[i] = binval[i];
	}
	buf[i] = '\0';
	return buf;
}

const char *dhcpoption_parseHex(unsigned char *binval, int len) {
	int i;
	if (len > (int)(sizeof(buf) - 1) / 2)
		len = (sizeof(buf) - 1) / 2;
	for (i = 0; i < len; i++)
		sprintf(&buf[i * 2], "%02x", binval[i]);
	buf[i] = '\0';
	return buf;
}

const char *dhcpoption_parseDefault(unsigned char *binval, int len) {
	int i;
	for (i=0; i<len; i++)
		if ((binval[i] < 32 || binval[i] > 126) &&
			((i != (len - 1)) || (binval[i] != 0)))
			break;
	if (!len)
		buf[0] = '\0';
	else if (i < len)
		dhcpoption_parseHex(binval, len);
	else
		dhcpoption_parseAscii(binval, len);
	return buf;
}

const char *dhcpoption_parseLLAddress(unsigned char *binval, int len) {
	int i;
	if (len > 256)
		len = 256;
	buf[0] = '\0';
	for (i=0; i<len; i++)
		sprintf(&buf[i*3], "%02x:", binval[i]);
	if (len)
		buf[len*3-1] = '\0';
	return buf;
}

const char *dhcpoption_parseDomainName(unsigned char *binval, int len, int *parsed) {
	int m, n;
	for (n = 0; binval[n] && n + binval[n] < len; n += m + 1) {
		m = binval[n];
		binval[n] = '.';
	}
	binval[n] = '\0';
	n++;
	if (parsed)
		*parsed = n;
	return (const char *)&binval[1];
}

int dhcpoption_getDomainName(const unsigned char *in_domain, int in_domain_len, char *out, int out_len, int offset, bool compressed)
{
	int written = 0;
	unsigned char n = 0;
	int start_offset = offset;


	while (1) {
		n = in_domain[offset++];

		if (n == 0)
			break; 

		if ((n & 0xc0) == 0xc0) {
			if (!compressed)
				return -1;

			int off = (n & 0x3f) | in_domain[offset++];
			if (dhcpoption_getDomainName(in_domain, in_domain_len, out+written, out_len - written, off, false) < 0)
				return -1;
			break;
		}

		if (written + n + 1 > out_len)
			return -1;
		
		memcpy(out + written, &in_domain[offset], n);
		written += n;
		offset += n; 

		if (in_domain[offset])
			out[written++] = '.';
		else 
			out[written++] = '\0';
	}

	return offset - start_offset;
}

bool dhcpoption_packIPv4Addr(unsigned char **binval, int *len, variant_t *val) {
	void *v;
	const string_t *str;

	if (variant_type(val) != variant_type_string)
		return false;
	str = variant_da_string(val);

	v = calloc(1, sizeof(struct in_addr));
	if (!v)
		return false;

	if (!inet_pton(AF_INET, string_buffer(str), v)) {
		free(v);
		return false;
	}

	*binval = (unsigned char *)v;
	*len = sizeof(struct in_addr);

	return true;
}

bool dhcpoption_packIPv6Addr(unsigned char **binval, int *len, variant_t *val) {
	void *v;
	const string_t *str;

	if (variant_type(val) != variant_type_string)
		return false;
	str = variant_da_string(val);

	v = calloc(1, sizeof(struct in6_addr));
	if (!v)
		return false;

	if (!inet_pton(AF_INET6, string_buffer(str), v)) {
		free(v);
		return false;
	}

	*binval = (unsigned char *)v;
	*len = sizeof(struct in6_addr);

	return true;
}

bool dhcpoption_packAscii(unsigned char **binval, int *len, variant_t *val) {
	unsigned char *v;
	const string_t *str;

	if (variant_type(val) != variant_type_string)
		return false;
	str = variant_da_string(val);

	v = calloc(1, string_length(str));
	if (!v)
		return false;

	memcpy(v, string_buffer(str), string_length(str));

	*binval = (unsigned char *)v;
	*len = string_length(str);

	return true;
}

bool dhcpoption_packHex(unsigned char **binval, int *len, variant_t *val) {
	unsigned int i;
	unsigned char *v;
	const string_t *str;

	if (variant_type(val) != variant_type_string)
		return false;
	str = variant_da_string(val);

	if (string_length(str)%2 != 0)
		return false;

	v = calloc(1, string_length(str)/2);
	if (!v)
		return false;

	for (i = 0; i < string_length(str); i += 2) {
		if (sscanf(string_buffer(str)+i, "%2hhx", &(v[i/2])) != 1) {
			free(v);
			return false;
		}
	}

	*binval = (unsigned char *)v;
	*len = string_length(str)/2;

	return true;
}

bool dhcpoption_packLLAddress(unsigned char **binval, int *len, variant_t *val) {
	unsigned int i;
	unsigned char *v;
	const string_t *str;

	if (variant_type(val) != variant_type_string)
		return false;
	str = variant_da_string(val);

	if (string_length(str)%3 != 2)
		return false;

	v = calloc(1, (string_length(str)+1)/3);
	if (!v)
		return false;

	for (i = 0; i < string_length(str); i += 3) {
		if (sscanf(string_buffer(str)+i, "%2hhx:", &(v[i/3])) != 1) {
			free(v);
			return false;
		}
	}

	*binval = (unsigned char *)v;
	*len = (string_length(str)+1)/3;

	return true;
}

bool dhcpoption_packDomainName(unsigned char **binval, int *len, variant_t *val) {
	unsigned int i;
	unsigned char *v;
	char *p;
	const string_t *str;

	if (variant_type(val) != variant_type_string)
		return false;
	str = variant_da_string(val);

	v = calloc(1, string_length(str)+2);
	if (!v)
		return false;

	memcpy(v+1, string_buffer(str), string_length(str));
	for (i = 0; i < string_length(str); i += v[i] + 1) {
		p = strchr((char *)(v+i+1), '.');
		if (p) {
			*p = 0;
		}

		v[i] = strlen((char *)(v+i+1));
	}

	*binval = (unsigned char *)v;
	*len = string_length(str)+2;

	return true;
}

bool dhcpoption_packInt8(unsigned char **binval, int *len, variant_t *val) {
	int8_t *v;

	if (variant_type(val) != variant_type_int8)
		return false;

	v = calloc(1, sizeof(int8_t));
	if (!v)
		return false;

	*v = variant_int8(val);

	*binval = (unsigned char *)v;
	*len = sizeof(int8_t);

	return true;
}

bool dhcpoption_packInt16(unsigned char **binval, int *len, variant_t *val) {
	int16_t *v;

	if (variant_type(val) != variant_type_int16)
		return false;

	v = calloc(1, sizeof(int16_t));
	if (!v)
		return false;

	*v = htons(variant_int16(val));

	*binval = (unsigned char *)v;
	*len = sizeof(int16_t);

	return true;
}

bool dhcpoption_packInt32(unsigned char **binval, int *len, variant_t *val) {
	int32_t *v;

	if (variant_type(val) != variant_type_int32)
		return false;

	v = calloc(1, sizeof(int32_t));
	if (!v)
		return false;

	*v = htonl(variant_int32(val));

	*binval = (unsigned char *)v;
	*len = sizeof(int32_t);

	return true;
}

bool dhcpoption_packInt64(unsigned char **binval, int *len, variant_t *val) {
	int64_t *v;

	if (variant_type(val) != variant_type_int64)
		return false;

	v = calloc(1, sizeof(int64_t));
	if (!v)
		return false;

	*v = htonll(variant_int64(val));

	*binval = (unsigned char *)v;
	*len = sizeof(int64_t);

	return true;
}

bool dhcpoption_packUInt8(unsigned char **binval, int *len, variant_t *val) {
	uint8_t *v;

	if (variant_type(val) != variant_type_uint8)
		return false;

	v = calloc(1, sizeof(uint8_t));
	if (!v)
		return false;

	*v = variant_uint8(val);

	*binval = (unsigned char *)v;
	*len = sizeof(uint8_t);

	return true;
}

bool dhcpoption_packUInt16(unsigned char **binval, int *len, variant_t *val) {
	uint16_t *v;

	if (variant_type(val) != variant_type_uint16)
		return false;

	v = calloc(1, sizeof(uint16_t));
	if (!v)
		return false;

	*v = htons(variant_uint16(val));

	*binval = (unsigned char *)v;
	*len = sizeof(uint16_t);

	return true;
}

bool dhcpoption_packUInt32(unsigned char **binval, int *len, variant_t *val) {
	uint32_t *v;

	if (variant_type(val) != variant_type_uint32)
		return false;

	v = calloc(1, sizeof(uint32_t));
	if (!v)
		return false;

	*v = htonl(variant_uint32(val));

	*binval = (unsigned char *)v;
	*len = sizeof(uint32_t);

	return true;
}

bool dhcpoption_packUInt64(unsigned char **binval, int *len, variant_t *val) {
	uint64_t *v;

	if (variant_type(val) != variant_type_uint64)
		return false;

	v = calloc(1, sizeof(uint64_t));
	if (!v)
		return false;

	*v = htonll(variant_uint64(val));

	*binval = (unsigned char *)v;
	*len = sizeof(uint64_t);

	return true;
}

bool dhcpoption_packDefault(unsigned char **binval, int *len, variant_t *val) {
	switch (variant_type(val)) {
		case variant_type_unknown:
			*binval = NULL;
			*len = 0;
			return true;
			break;
		case variant_type_string:
			return dhcpoption_packAscii(binval, len, val);
			break;
		case variant_type_int8:
			return dhcpoption_packInt8(binval, len, val);
			break;
		case variant_type_int16:
			return dhcpoption_packInt16(binval, len, val);
			break;
		case variant_type_int32:
			return dhcpoption_packInt32(binval, len, val);
			break;
		case variant_type_int64:
			return dhcpoption_packInt64(binval, len, val);
			break;
		case variant_type_uint8:
			return dhcpoption_packUInt8(binval, len, val);
			break;
		case variant_type_uint16:
			return dhcpoption_packUInt16(binval, len, val);
			break;
		case variant_type_uint32:
			return dhcpoption_packUInt32(binval, len, val);
			break;
		case variant_type_uint64:
			return dhcpoption_packUInt64(binval, len, val);
			break;
		case variant_type_bool:
		case variant_type_double:
		case variant_type_date_time:
		case variant_type_array:
		case variant_type_map:
		case variant_type_reference:
		case variant_type_array_reference:
		case variant_type_map_reference:
		case variant_type_file_descriptor:
		case variant_type_byte_array:
		default:
			break;
	}

	return false;
}
