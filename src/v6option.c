/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>

#include "util.h"
#include "dhcpoption.h"

static void dhcp_api_parseSubOptions(variant_list_t *list, int len, unsigned char *binval) {
	int i = 0;
	while (i + 4 <= len) {
		unsigned short type = dhcpoption_parseUInt16(&binval[i]);
		unsigned short olen = dhcpoption_parseUInt16(&binval[i+2]);
		if (i + 4 + olen > len)
			break;
		variant_map_t map;
		variant_map_initialize(&map);
		variant_map_addUInt16(&map, "Type", type);
		variant_t value;
		variant_initialize(&value, variant_type_string);
		dhcpoption_v6parse(&value, type, olen, &binval[i+4]);
		variant_map_add(&map, "Value", &value);
		variant_cleanup(&value);
		variant_list_addMap(list, &map);
		variant_map_cleanup(&map);
		i += 4 + olen;
	}
}

void dhcpoption_v6parse(variant_t *retval, uint16_t tag, int len, unsigned char *binval) {
	int i, n;
	switch (tag) {
		case   1: // Client Identifier
		case   2: // Server Identifier
		{
			if (len < 2)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			uint16_t type = dhcpoption_parseUInt16(binval);
			variant_map_addUInt16(&map, "Type", type);
			switch (type) {
				case 1: // DUID-LLT
					if (len < 8)
						break;
					variant_map_addUInt16(&map, "HardwareType", dhcpoption_parseUInt16(&binval[2]));
					variant_map_addUInt32(&map, "Time", dhcpoption_parseUInt32(&binval[4]));
					variant_map_addChar(&map, "LLAddress", dhcpoption_parseLLAddress(&binval[8], len - 8));
					break;
				case 2: // DUID-EN
					if (len < 6)
						break;
					variant_map_addUInt32(&map, "Enterprise", dhcpoption_parseUInt32(&binval[2]));
					variant_map_addChar(&map, "Identifier", dhcpoption_parseDefault(&binval[6], len - 6));
					break;
				case 3: // DUID-LL
					if (len < 4)
						break;
					variant_map_addUInt16(&map, "HardwareType", dhcpoption_parseUInt16(&binval[2]));
					variant_map_addChar(&map, "LLAddress", dhcpoption_parseLLAddress(&binval[4], len - 4));
					break;
				default:
					variant_map_addChar(&map, "Data", dhcpoption_parseDefault(&binval[2], len - 2));
					break;
			}
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case   3: // Identity Association for Non-temporary Addresses
		case  25: // Identity Association for Prefix Delegation
		{
			variant_map_t map;
			if (len < 12)
				break;
			variant_map_initialize(&map);
			variant_map_addUInt32(&map, "IAID", dhcpoption_parseUInt32(binval));
			variant_map_addUInt32(&map, "T1", dhcpoption_parseUInt32(&binval[4]));
			variant_map_addUInt32(&map, "T2", dhcpoption_parseUInt32(&binval[8]));
			variant_list_t list;
			variant_list_initialize(&list);
			dhcp_api_parseSubOptions(&list, len - 12, &binval[12]);
			variant_map_addArray(&map, "Options", &list);
			variant_list_cleanup(&list);
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case   4: // Identity Association for Temporary Addresses
		{
			variant_map_t map;
			if (len < 4)
				break;
			variant_map_initialize(&map);
			variant_map_addUInt32(&map, "IAID", dhcpoption_parseUInt32(binval));
			variant_list_t list;
			variant_list_initialize(&list);
			dhcp_api_parseSubOptions(&list, len - 4, &binval[4]);
			variant_map_addArray(&map, "Options", &list);
			variant_list_cleanup(&list);
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case   5: // IA Address
		{
			variant_map_t map;
			if (len < 24)
				break;
			variant_map_initialize(&map);
			variant_map_addChar(&map, "Address", dhcpoption_parseIPv6Addr(binval));
			variant_map_addUInt32(&map, "PreferredLifetime", dhcpoption_parseUInt32(&binval[16]));
			variant_map_addUInt32(&map, "ValidLifetime", dhcpoption_parseUInt32(&binval[20]));
			variant_list_t list;
			variant_list_initialize(&list);
			dhcp_api_parseSubOptions(&list, len - 24, &binval[24]);
			variant_map_addArray(&map, "Options", &list);
			variant_list_cleanup(&list);
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case   6: // Option Request
		case  43: // Echo Request
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i = 0; i + 2 <= len; i += 2)
				variant_list_addUInt16(&list, dhcpoption_parseUInt16(&binval[i]));
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case   7: // Preference
		case  19: // Reconfigure Message
			if (len < 1)
				break;
			variant_setUInt8(retval, binval[0]);
			break;
		case   8: // Elapsed Time
			if (len < 2)
				break;
			variant_setUInt16(retval, dhcpoption_parseUInt16(binval));
			break;
		case   9: // Relay Message
			variant_setChar(retval, dhcpoption_parseHex(binval, len));
			break;
		case  11: // Authentication
		{
			if (len < 11)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt8(&map, "Protocol", binval[0]);
			variant_map_addUInt8(&map, "Algorithm", binval[1]);
			variant_map_addUInt8(&map, "RDM", binval[2]);
			variant_map_addChar(&map, "ReplayDetection", dhcpoption_parseDefault(&binval[3], 8));
			variant_map_addChar(&map, "AuthenticationInformation", dhcpoption_parseDefault(&binval[11], len - 11));
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case  12: // Server Unicast
			if (len < 16)
				break;
			variant_setChar(retval, dhcpoption_parseIPv6Addr(binval));
			break;
		case  13: // Status Code
		{
			if (len < 2)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt16(&map, "Code", dhcpoption_parseUInt16(binval));
			variant_map_addChar(&map, "Message", (char *)&binval[2]);
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case  14: // Rapid Commit
		case  20: // Reconfigure Accept
			variant_setBool(retval, true);
			break;
		case  15: // User Class
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i=0; i + 2 <= len && i + 2 + dhcpoption_parseUInt16(&binval[i]) <= len; i += 2 + dhcpoption_parseUInt16(&binval[i])) {
				variant_list_addChar(&list, dhcpoption_parseDefault(&binval[i+2], dhcpoption_parseUInt16(&binval[i])));
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case  16: // Vendor Class
		{
			if (len < 4)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt32(&map, "Enterprise", dhcpoption_parseUInt32(binval));
			variant_list_t list;
			variant_list_initialize(&list);
			for (i = 4; i + 2 <= len && i + 2 + dhcpoption_parseUInt16(&binval[i]) <= len; i += 2 + dhcpoption_parseUInt16(&binval[i])) {
				variant_list_addChar(&list, dhcpoption_parseDefault(&binval[i+2], dhcpoption_parseUInt16(&binval[i])));
			}
			variant_map_addArray(&map, "Data", &list);
			variant_list_cleanup(&list);
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case  17: // Vendor-specific Information
		{
			if (len < 4)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt32(&map, "Enterprise", dhcpoption_parseUInt32(binval));
			variant_list_t list;
			variant_list_initialize(&list);
			for (i = 4; i + 4 <= len && i + 4 + dhcpoption_parseUInt16(&binval[i+2]) <= len; i += 4 + dhcpoption_parseUInt16(&binval[i+2])) {
				variant_map_t map2;
				variant_map_initialize(&map2);
				variant_map_addUInt8(&map2, "Code", dhcpoption_parseUInt16(&binval[i]));
				variant_map_addChar(&map2, "Data", dhcpoption_parseDefault(&binval[i+4], dhcpoption_parseUInt16(&binval[i+2])));
				variant_list_addMap(&list, &map2);
				variant_map_cleanup(&map2);
			}
			variant_map_addArray(&map, "Data", &list);
			variant_list_cleanup(&list);
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case  21: // SIP Servers Domain Name List
		case  24: // Domain Search List
		case  33: // Broadcast and Multicast Service Controller Domain Name List
		case  58: // SIP User Agent Configuration Service Domains
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i = 0; i < len; i += n)
				variant_list_addChar(&list, dhcpoption_parseDomainName(&binval[i], len - i, &n));
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case  22: // SIP Servers IPv6 Address List
		case  23: // DNS Recursive Name Server
		case  27: // Network Information Service (NIS) Servers
		case  28: // Network Information Service V2 (NIS+) Servers
		case  31: // Simple Network Time Protocol (SNTP) Servers
		case  34: // Broadcast and Multicast Service Controller IPv6 Address
		case  40: // PANA Authentication Agent
		case  48: // Leasequery Client Link
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i = 0; i + 16 <= len; i += 16) {
				variant_list_addChar(&list, dhcpoption_parseIPv6Addr(&binval[i]));
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case  26: // IA_PD Prefix
		{
			if (len < 25)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt32(&map, "PreferredLifetime", dhcpoption_parseUInt32(binval));
			variant_map_addUInt32(&map, "ValidLifetime", dhcpoption_parseUInt32(&binval[4]));
			variant_map_addUInt8(&map, "PrefixLen", binval[8]);
			variant_map_addChar(&map, "Prefix", dhcpoption_parseIPv6Addr(&binval[9]));
			variant_list_t list;
			variant_list_initialize(&list);
			dhcp_api_parseSubOptions(&list, len - 25, &binval[25]);
			variant_map_addArray(&map, "Options", &list);
			variant_list_cleanup(&list);
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case  29: // Network Information Service (NIS) Domain Name
		case  30: // Network Information Service V2 (NIS+) Domain Name
		case  51: // LoST server
		case  64: // AFTR-Name for DSLite
			variant_setChar(retval, dhcpoption_parseDomainName(binval, len, NULL));
			break;
		case  32: // Information Refresh Time
		case  46: // Leasequery Client Transaction time
			if (len < 4)
				break;
			variant_setUInt32(retval, dhcpoption_parseUInt32(binval));
			break;
		case  36: // Civic Addresses Configuration Information
		{
			if (len < 3)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt8(&map, "What", binval[0]);
			char country[3] = { (char)binval[1], (char)binval[2], '\0' };
			variant_map_addChar(&map, "Country", country);
			variant_list_t list;
			variant_list_initialize(&list);
			for (i = 3; i + 2 <= len && i + 2 + binval[i+1] <= len; i += 2 + binval[i+1]) {
				variant_map_t map2;
				variant_map_initialize(&map2);
				variant_map_addUInt8(&map2, "Type", binval[i]);
				variant_map_addChar(&map2, "Value", dhcpoption_parseAscii(&binval[i+2], binval[i+1]));
				variant_list_addMap(&list, &map2);
				variant_map_cleanup(&map2);
			}
			variant_map_addArray(&map, "Address", &list);
			variant_list_cleanup(&list);
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case 37: // Relay Agent Remote-ID
		{
			if (len < 5)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt32(&map, "Enterprise", dhcpoption_parseUInt32(binval));
			variant_map_addChar(&map, "RemoteID", dhcpoption_parseDefault(&binval[4], len - 4));
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case 39: // Client FQDN
		{
			if (len < 1)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt8(&map, "Flags", binval[0]);
			variant_map_addChar(&map, "DomainName", dhcpoption_parseDomainName(&binval[1], len - 1, NULL));
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case  41: // Timezone POSIX String
		case  42: // Timezone Name
			variant_setChar(retval, (char *)binval);
			break;
		case  44: // Leasequery Query
		{
			if (len < 17)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt8(&map, "Type", binval[0]);
			variant_map_addChar(&map, "LinkAddress", dhcpoption_parseIPv6Addr(&binval[1]));
			variant_list_t list;
			variant_list_initialize(&list);
			dhcp_api_parseSubOptions(&list, len - 17, &binval[17]);
			variant_map_addArray(&map, "Options", &list);
			variant_list_cleanup(&list);
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case  45: // Leasequery Client Data
		{
			variant_list_t list;
			variant_list_initialize(&list);
			dhcp_api_parseSubOptions(&list, len, binval);
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case  47: // Leasequery Relay Data
		{
			if (len < 16)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addChar(&map, "PeerAddress", dhcpoption_parseIPv6Addr(binval));
			variant_map_addChar(&map, "Message", dhcpoption_parseHex(&binval[16], len - 16));
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case  18: // Interface-Id
		case  38: // Relay Agent Subscriber-ID
		default: // unknown option - simple ascii string if all bytes between 32 and 126, keep hexbinary prefixed with 0x otherwise
			variant_setChar(retval, dhcpoption_parseDefault(binval, len));
			break;
	}
}

bool dhcpoption_v6allowMultiple(uint8_t tag) {
	switch (tag) {
		case   3: // Identity Association for Non-temporary Addresses
		case   4: // Identity Association for Temporary Addresses
		case  17: // Vendor-specific Information
		case  25: // Identity Association for Prefix Delegation
			return true;
		default:
			return false;
	}
}

bool dhcpoption_v6pack(unsigned char **binval, int *len, uint8_t tag, variant_t *val) {
	bool ret;

	if (!binval || !len || !val) {
		return false;
	}

	switch (tag) {
		default:
			ret = dhcpoption_packDefault(binval, len, val);
			break;
	}

	return ret;
}
