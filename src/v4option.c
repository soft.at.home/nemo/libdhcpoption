/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>

#include "util.h"

void dhcpoption_v4parse(variant_t *retval, uint8_t tag, int len, unsigned char *binval) {
	int i, m, n;
	switch (tag) {
		case   1: // Subnet Mask
		case  16: // Swap Server
		case  28: // Broadcast Address
		case  32: // Router Solicitation Address
		case  50: // Requested IP Address
		case  54: // Server Identifier
			if (len < 4)
				break;
			variant_setChar(retval, dhcpoption_parseIPv4Addr(binval));
			break;
		case   2: // Time Offset
			if (len < 4)
				break;
			variant_setInt32(retval, dhcpoption_parseInt32(binval));
			break;
		case   3: // Router
		case   4: // Time Server
		case   5: // Name Server
		case   6: // Domain Name Server
		case   7: // Log Server
		case   8: // Cookie Server
		case   9: // LPR Server
		case  10: // Impress Server
		case  11: // Resource Location Server
		case  41: // Network Information Servers
		case  42: // Network Time Protocol Servers
		case  44: // NetBIOS over TCP/IP Name Server
		case  45: // NetBIOS over TCP/IP Datagram Distribution Server
		case  48: // X Window System Font Server
		case  49: // X Window System Display Manager
		case  65: // Network Information Service+ Servers
		case  68: // Mobile IP Home Agent
		case  69: // Simple Mail Transport Protocol (SMTP) Server
		case  70: // Post Office Protocol (POP3) Server
		case  71: // Network News Transport Protocol (NNTP) Server
		case  72: // Default World Wide Web (WWW) Server
		case  73: // Default Finger Server
		case  74: // Default Internet Relay Chat (IRC) Server
		case  75: // StreetTalk Server
		case  76: // StreetTalk Directory Assistance (STDA) Server
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i=0; i+4<=len; i+=4) {
				variant_list_addChar(&list, dhcpoption_parseIPv4Addr(&binval[i]));
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case  21: // Policy Filter
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i=0; i+8<=len; i+=8) {
				variant_map_t map;
				variant_map_initialize(&map);
				variant_map_addChar(&map, "Address", dhcpoption_parseIPv4Addr(&binval[i]));
				variant_map_addChar(&map, "Mask", dhcpoption_parseIPv4Addr(&binval[i + 4]));
				variant_list_addMap(&list, &map);
				variant_map_cleanup(&map);
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case  33: // Static Route
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i=0; i+8<=len; i+=8) {
				variant_map_t map;
				variant_map_initialize(&map);
				variant_map_addChar(&map, "Destination", dhcpoption_parseIPv4Addr(&binval[i]));
				variant_map_addChar(&map, "Router", dhcpoption_parseIPv4Addr(&binval[i + 4]));
				variant_list_addMap(&list, &map);
				variant_map_cleanup(&map);
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case  13: // Boot File Size
		case  22: // Maximum Datagram Reassembly Size
		case  26: // Interface MTU
		case  57: // Maximum DHCP Message Size
			if (len < 2)
				break;
			variant_setUInt16(retval, dhcpoption_parseUInt16(binval));
			break;
		case  19: // IP Forwarding Enable/Disable
		case  20: // Non-Local Source Routing Enable/Disable
		case  27: // All Subnets are Local Option
		case  29: // Perform Mask Discovery
		case  30: // Mask Supplier
		case  31: // Perform Router Discovery
		case  34: // Trailer Encapsulation
		case  36: // Ethernet Encapsulation
		case  39: // TCP Keepalive Garbage
			if (len < 1)
				break;
			variant_setBool(retval, binval[0]);
			break;
		case  23: // Default IP Time-to-live
		case  37: // TCP Default TTL
		case  52: // Option Overload
		case  53: // DHCP Message Type
			if (len < 1)
				break;
			variant_setUInt8(retval, binval[0]);
			break;
		case  24: // Path MTU Aging Timeout
		case  35: // ARP Cache Timeout
		case  38: // TCP Keepalive Interval
		case  51: // IP Address Lease Time
		case  58: // Renewal (T1) Time Value
		case  59: // Rebinding (T2) Time Value
			if (len < 4)
				break;
			variant_setUInt32(retval, dhcpoption_parseUInt32(binval));
			break;
		case  25: // Path MTU Plateau Table
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i=0; i+2<=len; i+=2) {
				variant_list_addUInt16(&list, dhcpoption_parseUInt16(&binval[i]));
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case  12: // Host Name
		case  14: // Merit Dump File
		case  15: // Domain Name
		case  17: // Root Path
		case  18: // Extensions Path
		case  40: // Network Information Service Domain
		case  47: // NetBIOS over TCP/IP Scope
		case  64: // Network Information Service+ Domain
		case  66: // TFTP server name
		case  67: // Bootfile name
		case  56: // Message
			variant_setChar(retval, dhcpoption_parseAscii(binval, len));
			break;
		case  43: // Vendor Specific Information
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i = 0; i + 2 <= len && binval[i] != 255 && i + 2 + binval[i+1] <= len; i += 2 + binval[i+1]) {
				variant_map_t map;
				variant_map_initialize(&map);
				variant_map_addUInt8(&map, "Code", binval[i]);
				variant_map_addChar(&map, "Data", dhcpoption_parseDefault(&binval[i+2], binval[i+1]));
				variant_list_addMap(&list, &map);
				variant_map_cleanup(&map);
			}
			if (i < len && binval[i] == 255)
				i++;
			if (i < len)
				variant_list_addChar(&list, dhcpoption_parseDefault(&binval[i], len - i));
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case  55: // Parameter Request List - probably useless but just for completeness ...
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i=0; i<len; i++) {
				variant_list_addUInt8(&list, binval[i]);
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case  61: // Client-identifier
		{
			if (len < 1)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt8(&map, "Type", binval[0]);
			if (binval[0] == 48 && len == 7) { // any kind of hardware address
                                variant_map_addChar(&map, "ClientIdentifier", dhcpoption_parseLLAddress(&binval[1], len - 1));
                        }
			else { // unique identifier other than a hardware address
				variant_map_addChar(&map, "ClientIdentifier", dhcpoption_parseDefault(&binval[1], len - 1));
                        }
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case  77: // User class identifier
		{
			variant_list_t list;
			variant_list_initialize(&list);
			unsigned char s_len = binval[0];
			if (s_len > 0 && s_len < len) {
				/* option 77 allows a list of suboptions, 
				   each suboption starts with a lenght, 
				   followed by a suboption of that lenght. */
				for (i=0; i < len && i + 1 + binval[i] <= len; i += 1 + binval[i]) {
					variant_list_addChar(&list, dhcpoption_parseDefault(&binval[i+1], binval[i]));
				}
			}
			else {
				/* option 77 is not correctly configured. the option does not 
				 start with a lenght field. 
				 we allow it here for compatibility with older STB's */
				/* filter out non printable chars */
				unsigned char *s = calloc(1, len);
				int j;
				int slen = 0;
				for (j=0; j<len; j++) {
					if (isprint(binval[j])) {
						s[slen] = binval[j];
						slen++;
					}
				}
				variant_list_addChar(&list, dhcpoption_parseDefault(s, slen));
				free(s);
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
			
		}
		case  90: // Authentication
		{
			if (len < 11)
				break;
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt8(&map, "Protocol", binval[0]);
			variant_map_addUInt8(&map, "Algorithm", binval[1]);
			variant_map_addUInt8(&map, "RDM", binval[2]);
			variant_map_addChar(&map, "ReplayDetection", dhcpoption_parseDefault(&binval[3], 8));
			variant_map_addChar(&map, "AuthenticationInformation", dhcpoption_parseDefault(&binval[11], len - 11));
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case 119: //Domain search
		{
			variant_list_t list;
			int res = 0;
			i = 0;
			char domain[256];
			if (len < 1)
				break;
			variant_list_initialize(&list);
			do {
				res = dhcpoption_getDomainName(binval, len, domain, sizeof(domain), i, true);
				if (res <= 0)
					break;
				variant_list_addChar(&list, domain);
				i += res;
			} while (i < len);

			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case 120: // Session Initiation Protocol (SIP) Servers
		{
			if (len < 1)
				break;
			variant_list_t list;
			variant_list_initialize(&list);
			if (binval[0] == 0) { // list of DNS names
				for (i=1; i<len; i+=n) {
					variant_list_addChar(&list, dhcpoption_parseDomainName(&binval[i], len - i, &n));
				}
			} else if (binval[0] == 1) { // list of IPv4 Addresses
				for (i=1; i+4<=len; i+=4) {
					variant_list_addChar(&list, dhcpoption_parseIPv4Addr(&binval[i]));
				}
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case 121: // Classless Static Route
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i = 0; i + 1 <= len; i += 4) {
				if (binval[i] > 32 || i + (binval[i] + 7) / 8 + 4 >= len) // corrupt option format
					break;
				unsigned char dst[4] = {'\0', '\0', '\0', '\0'};
				if (binval[i])
					memcpy(&dst, &binval[i+1], (binval[i] + 7) / 8);
				if (binval[i] % 8)
					dst[(binval[i] - 1) / 8] &= 0xff00 >> (binval[i] % 8);
				variant_map_t map;
				variant_map_initialize(&map);
				variant_map_addChar(&map, "Destination", dhcpoption_parseIPv4Addr(dst));
				variant_map_addUInt8(&map, "PrefixLength", binval[i]);
				i += 1 + (binval[i] + 7) / 8;
				variant_map_addChar(&map, "Router", dhcpoption_parseIPv4Addr(&binval[i]));
				variant_list_addMap(&list, &map);
				variant_map_cleanup(&map);
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case 124: // Vendor-Identifying Vendor Class
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i = 0; i + 5 <= len && i + 5 + binval[i+4] <= len; i += 5 + binval[i+4]) {
				variant_map_t map;
				variant_map_initialize(&map);
				variant_map_addUInt32(&map, "Enterprise", dhcpoption_parseUInt32(&binval[i]));
				variant_map_addChar(&map, "Data", dhcpoption_parseDefault(&binval[i+5], binval[i+4]));
				variant_list_addMap(&list, &map);
				variant_map_cleanup(&map);
			}
			if (i < len && binval[i] == 255)
				i++;
			if (i < len)
				variant_list_addChar(&list, dhcpoption_parseDefault(&binval[i], len - i));
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case 125: // Vendor-Identifying Vendor-Specific Information
		{
			variant_list_t list;
			variant_list_initialize(&list);
			for (i = 0; i + 5 <= len && i + 5 + binval[i+4] <= len; i += 5 + binval[i+4]) {
				variant_map_t map;
				variant_map_initialize(&map);
				variant_map_addUInt32(&map, "Enterprise", dhcpoption_parseUInt32(&binval[i]));

				variant_list_t sublist;
				variant_list_initialize(&sublist);
				for (m = 0; m + 2 <= binval[i+4] && m + 2 + binval[i+5+m+1] <= binval[i+4]; m += 2 + binval[i+5+m+1]) {
					variant_map_t submap;
					variant_map_initialize(&submap);
					variant_map_addUInt8(&submap, "Code", binval[i+5+m]);
					variant_map_addChar(&submap, "Data", dhcpoption_parseDefault(&binval[i+5+m+2], binval[i+5+m+1]));
					variant_list_addMap(&sublist, &submap);
					variant_map_cleanup(&submap);
				}
				if (m < binval[i+4])
					variant_list_addChar(&sublist, dhcpoption_parseDefault(&binval[i+5+m], binval[i+4] - m));
				variant_map_addArray(&map, "Data", &sublist);
				variant_list_cleanup(&sublist);

				variant_list_addMap(&list, &map);
				variant_map_cleanup(&map);
			}
			variant_setArrayMove(retval, &list);
			variant_list_cleanup(&list);
			break;
		}
		case 212: //6rd option
		{
			if (len<22) {
				break;
			}
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt32(&map, "IPv4MaskLen", binval[0]);
			variant_map_addUInt32(&map, "6rdPrefixLen", binval[1]);
			variant_map_addChar(&map, "6rdPrefix", dhcpoption_parseIPv6Addr(&binval[2]));

			variant_list_t sublist;
			variant_list_initialize(&sublist);
			i = 0;
			while (18 + (i*4) <= len) {
				variant_list_addChar(&sublist, dhcpoption_parseIPv4Addr(&binval[18 + (i*4)]));
				i++;
			}
			variant_map_addArray(&map, "6rdBRIPv4Address", &sublist);
			variant_list_cleanup(&sublist);

			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case 252: // Configure Mqtt server option
		{
			if (len<2) {
				break;
			}
			variant_map_t map;
			variant_map_initialize(&map);
			variant_map_addUInt32(&map, "SubOption0", binval[0]);
			variant_map_addUInt32(&map, "Networktype", binval[1]);
			variant_setMapMove(retval, &map);
			variant_map_cleanup(&map);
			break;
		}
		case  46: // NetBIOS over TCP/IP Node Type Option - it has a specific format but who cares?
		case  60: // Vendor class identifier
		default: // unknown option - simple ascii string if all bytes between 32 and 126, keep hexbinary prefixed with 0x otherwise
			variant_setChar(retval, dhcpoption_parseDefault(binval, len));
			break;
	}
}

bool dhcpoption_v4pack(unsigned char **binval, int *len, uint8_t tag, variant_t *val) {
	bool ret;

	if (!binval || !len || !val) {
		return false;
	}

	switch (tag) {
		default:
			ret = dhcpoption_packDefault(binval, len, val);
			break;
	}

	return ret;
}
