# libdhcpoption

## Summary

Library to parse and evaluate the binary data of DHCP options.

## Description

The DHCP protocol allows to send and receive DHCP options.
These provide important additional data regarding the network
settings of where the host is connected to.
The format of the DHCP options is depending on the option identifier.
The value is usually provided in a binary format that requires parsing.

This library provides an API to parse the binary data of the DHCP
option values.
It returns the same data in a `variant_t` data format.
